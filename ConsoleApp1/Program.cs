﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using SpreadsheetLight;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Diagnostics;

namespace SQL_QUERY_CONOSOLE
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("start");
            string queryString = Console.ReadLine();
            string saveExcelPath = @"D:\DEV\SQL_Query_Results\Test.xlsx";

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            string connectionString = @"Data Source = (DESCRIPTION =
                    (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))
                    (CONNECT_DATA = 
                    (SERVER = DEDICATED)
                    (SERVICE_NAME = orcl)
                )
            ); User Id = system; password = 1;";

            string query = "select * from test_data1 t";



            OracleConnection con = null;
            OracleDataReader dr = null;
            try
            {
                SLDocument sl = new SLDocument();
                int j = 0,
                    rowStart = 0,
                    rowEnd = 1000,
                    rowCnt = 0;
                con = new OracleConnection();
                con.ConnectionString = connectionString;
                con.Open();

                while (rowCnt < 1000000)
                {
                    OracleCommand cmd = new OracleCommand();
                    cmd.CommandText = $"select * from (select rownum rnum, a.* from ({query}) a where rownum <= {rowEnd} order by rowid) where rnum >= {rowStart}";
                    cmd.Connection = con;
                    cmd.CommandType = System.Data.CommandType.Text;
                    dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        for (int i = 1; i < dr.FieldCount + 1; i++)
                        {
                            string clmnName = dr.GetName(i - 1);
                            sl.SetCellValue(j + 2, i, dr[clmnName].ToString());
                            if (j == 0)
                            {
                                sl.SetCellValue(j + 1, i, dr.GetName(i - 1));
                            }
                        }
                        j++;
                    }
                    Console.WriteLine(rowCnt);
                    rowEnd = rowEnd + 1000;
                    rowStart = rowStart + 1000;
                    rowCnt = rowCnt + 1000;
                    dr.Close();
                    dr.Dispose();
                    
                }
                sl.SaveAs(saveExcelPath);
                stopwatch.Stop();
                Console.WriteLine(stopwatch.Elapsed);
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }

        }
    }
}

